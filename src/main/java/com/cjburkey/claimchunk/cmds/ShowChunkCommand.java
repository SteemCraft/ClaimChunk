package com.cjburkey.claimchunk.cmds;

import com.cjburkey.claimchunk.chunk.ChunkPos;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ShowChunkCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            ChunkPos p = new ChunkPos(player.getLocation().getChunk());
            p.outlineChunk(player);
        }
        return true;
    }
}
