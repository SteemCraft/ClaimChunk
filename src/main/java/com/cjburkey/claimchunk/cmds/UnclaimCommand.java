package com.cjburkey.claimchunk.cmds;

import com.cjburkey.claimchunk.ClaimChunk;
import com.cjburkey.claimchunk.Config;
import com.cjburkey.claimchunk.Utils;
import com.cjburkey.claimchunk.chunk.ChunkHandler;
import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;

public class UnclaimCommand implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            try {
                unclaimChunk(player);
            } catch (IOException e) {
                e.printStackTrace();
                Utils.msg(player, Config.getColor("errorColor") + "An error occurred while unclaiming that chunk.");
            }
        }
        return true;
    }

    private void unclaimChunk(Player p) throws IOException {
        ChunkHandler ch = ClaimChunk.getInstance().getChunkHandler();
        Chunk loc = p.getLocation().getChunk();
        if(!ch.isClaimed(loc.getWorld(), loc.getX(), loc.getZ())) {
            Utils.toPlayer(p, Config.getColor("errorColor"), Utils.getMsg("unclaimNotOwned"));
            return;
        }
        if(!ch.isOwner(loc.getWorld(), loc.getX(), loc.getZ(), p)) {
            Utils.toPlayer(p, Config.getColor("errorColor"), Utils.getMsg("unclaimNotOwner"));
            return;
        }
        ch.unclaimChunk(loc.getWorld(), loc.getX(), loc.getZ());
        Utils.toPlayer(p, Config.getColor("successColor"), Utils.getMsg("unclaimSuccess"));
    }
}
