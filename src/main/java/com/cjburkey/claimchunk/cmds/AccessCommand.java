package com.cjburkey.claimchunk.cmds;

import com.cjburkey.claimchunk.ClaimChunk;
import com.cjburkey.claimchunk.Config;
import com.cjburkey.claimchunk.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.UUID;

public class AccessCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            try {
                accessChunk(player, strings);
            } catch (IOException e) {
                e.printStackTrace();
                Utils.msg(player, Config.getColor("errorColor") + "There was an error while editing that chunk.");
            }
        }
        return true;
    }

    private void accessChunk(Player p, String[] args) throws IOException {
        Player other = ClaimChunk.getInstance().getServer().getPlayer(args[0]);
        if (other != null) {
            toggle(p, other.getUniqueId(), other.getName());
        } else {
            UUID otherId = ClaimChunk.getInstance().getPlayerHandler().getUUID(args[0]);
            if (otherId == null) {
                Utils.toPlayer(p, Config.getColor("errorColor"), Utils.getMsg("accessNoPlayer"));
                return;
            }
            toggle(p, otherId, args[0]);
        }
    }

    private void toggle(Player owner, UUID other, String otherName) throws IOException {
        if (owner.getUniqueId().equals(other)) {
            Utils.toPlayer(owner, Config.getColor("errorColor"), Utils.getMsg("accessOneself"));
            return;
        }
        boolean hasAccess = ClaimChunk.getInstance().getPlayerHandler().toggleAccess(owner.getUniqueId(), other);
        if (hasAccess) {
            Utils.toPlayer(owner, Config.getColor("successColor"), Utils.getMsg("accessHas").replaceAll("%%PLAYER%%", otherName));
            return;
        }
        Utils.toPlayer(owner, Config.getColor("successColor"), Utils.getMsg("accessNoLongerHas").replaceAll("%%PLAYER%%", otherName));
    }
}
