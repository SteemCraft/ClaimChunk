package com.cjburkey.claimchunk.packet;

import java.lang.reflect.Constructor;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

/**
 * Class to handle particle effects using reflection.
 * Hopefully version independent.
 * @author cjburkey
 */
public final class ParticleHandler {
	
	/**
	 * Spawns (particle) at (loc), visible to all of (forPlayer)
	 * @param loc The location at which to display the particle.
	 * @param particle The particle to display.
	 * @param players The player(s) for whom to display the particles.
	 */
	public static void spawnParticleForPlayers(Location loc, Particle particle, Player... players) {
		try {
			spawnParticle(loc, players, particle);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void spawnParticle(Location loc, Player[] players, Particle particle) throws Exception {
		Class<?> particleClass = PacketHandler.getNMSClass("PacketPlayOutWorldParticles");
		Class<?> particleEnum = PacketHandler.getNMSClass("EnumParticle");
		
		Class<?>[] parameters = new Class<?>[] {
			particleEnum, boolean.class, float.class, float.class, float.class, float.class, float.class, float.class, float.class, int.class, int[].class
		};
		Object[] arguments = new Object[] {
			particleEnum.getField(particle.name()).get(null), true, (float) loc.getX(), (float) loc.getY(), (float) loc.getZ(), 0f, 0f, 0f, 1f, 0, new int[0]
		};
		
		Constructor<?> constructor = particleClass.getConstructor(parameters);
		Object packet = constructor.newInstance(arguments);
		
		for (Player p : players) {
			PacketHandler.sendPacket(p, packet);
		}
	}
	
}