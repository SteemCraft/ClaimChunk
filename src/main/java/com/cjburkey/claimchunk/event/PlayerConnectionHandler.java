package com.cjburkey.claimchunk.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import com.cjburkey.claimchunk.ClaimChunk;

public class PlayerConnectionHandler implements Listener {
	
	@EventHandler
	void onPlayerJoin(PlayerJoinEvent e) {
		ClaimChunk.getInstance().getPlayerHandler().onJoin(e.getPlayer());
	}
	
}