package com.cjburkey.claimchunk.event;

import com.cjburkey.claimchunk.*;
import org.bukkit.Chunk;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMovementHandler implements Listener {
	
	@EventHandler
	void onPlayerMove(PlayerMoveEvent e) {
		if (e != null && e.getPlayer() != null && !e.isCancelled()) {
			Chunk prev = e.getFrom().getChunk();
			Chunk to = e.getTo().getChunk();
			if (prev != to) {
				ClaimChunk.getInstance().showTitle(e.getPlayer(), to);
			}
		}
	}
	
}
