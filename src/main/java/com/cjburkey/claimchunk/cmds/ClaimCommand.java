package com.cjburkey.claimchunk.cmds;

import com.cjburkey.claimchunk.ClaimChunk;
import com.cjburkey.claimchunk.Config;
import com.cjburkey.claimchunk.Utils;
import com.cjburkey.claimchunk.chunk.ChunkHandler;
import com.cjburkey.claimchunk.chunk.ChunkPos;
import org.bukkit.Chunk;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;

public class ClaimCommand implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            claimChunk(player, player.getLocation().getChunk());
        }
        return true;
    }

    private void claimChunk(Player p, Chunk loc) {
        try {
            ChunkHandler ch = ClaimChunk.getInstance().getChunkHandler();
            if (ch.isClaimed(loc.getWorld(), loc.getX(), loc.getZ())) {
                Utils.toPlayer(p, Config.getColor("errorColor"), Utils.getMsg("claimAlreadyOwned"));
                return;
            }
            double cost = Config.getDouble("economy", "claimPrice");
            if (cost > 0) {
                double playerBalance = ClaimChunk.getInstance().economy.getBalance(p);
                if(playerBalance < cost){
                    Utils.toPlayer(p, Config.getColor("errorColor"), Utils.getMsg("claimNotEnoughMoney"));
                    return;
                }
            }
            int max = Config.getInt("chunks", "maxChunksClaimed");
            if (max > 0) {
                if (ch.getClaimed(p.getUniqueId()) > max) {
                    Utils.toPlayer(p, Config.getColor("errorColor"), Utils.getMsg("claimTooMany"));
                    return;
                }
            }
            ChunkPos pos = ch.claimChunk(loc.getWorld(), loc.getX(), loc.getZ(), p.getUniqueId());
            pos.outlineChunk(p);

            //TODO: send 25mSBD to @steemcraft.com savings. (steemquest plugin)
            ClaimChunk.getInstance().economy.withdrawPlayer(p, cost);
            Utils.toPlayer(p, Config.getColor("successColor"), Utils.getMsg("claimSuccess"));
        } catch (IOException e) {
            e.printStackTrace();
            Utils.msg(p, Config.getColor("errorColor") + "There was an error while claiming that chunk.");
        }
    }
}
