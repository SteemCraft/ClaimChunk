package com.cjburkey.claimchunk;

import java.io.File;
import java.util.UUID;

import com.cjburkey.claimchunk.cmds.AccessCommand;
import com.cjburkey.claimchunk.cmds.ClaimCommand;
import com.cjburkey.claimchunk.cmds.ShowChunkCommand;
import com.cjburkey.claimchunk.cmds.UnclaimCommand;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import com.cjburkey.claimchunk.chunk.ChunkHandler;
import com.cjburkey.claimchunk.event.CancellableChunkEvents;
import com.cjburkey.claimchunk.event.PlayerConnectionHandler;
import com.cjburkey.claimchunk.event.PlayerMovementHandler;
import com.cjburkey.claimchunk.player.PlayerHandler;

public final class ClaimChunk extends JavaPlugin {
	
	private static ClaimChunk instance;

	private File chunkFile;
	private File plyFile;

    public Economy economy = null;
	private ChunkHandler chunkHandler;
	private PlayerHandler playerHandler;
	
	public void onLoad() {
		instance = this;
	}
	
	public void onEnable() {
		chunkFile = new File(getDataFolder(), "/data/claimedChunks.json");
		plyFile = new File(getDataFolder(), "/data/playerData.json");

        setupEconomy();
		boolean useSql = false;
		playerHandler = new PlayerHandler(useSql, plyFile);
		chunkHandler = new ChunkHandler(useSql, chunkFile);

		setupConfig();
		Utils.log("Config set up.");
		
		setupCommands();
		Utils.log("Commands set up.");
		
		setupEvents();
		Utils.log("Events set up.");
		
		try {
			chunkHandler.readFromDisk();
			playerHandler.readFromDisk();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Utils.log("Loaded data.");
		
		scheduleDataSaver();
		Utils.log("Scheduled data saving.");
		
		Utils.log("Initialization complete.");

		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            for(Player p : Bukkit.getOnlinePlayers()){
            	showTitle(p, p.getLocation().getChunk());
			}
        }, 20L, 20L);
	}
	
	public void onDisable() {
		try {
			chunkHandler.writeToDisk();
			playerHandler.writeToDisk();
			Utils.log("Saved data.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		Utils.log("Finished disable.");
	}
	
	private void setupConfig() {
		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
	}
	
	private void setupEvents() {
		getServer().getPluginManager().registerEvents(new PlayerConnectionHandler(), this);
		getServer().getPluginManager().registerEvents(new CancellableChunkEvents(), this);
		getServer().getPluginManager().registerEvents(new PlayerMovementHandler(), this);
	}
	
	private void setupCommands() {
		getCommand("claim").setExecutor(new ClaimCommand());
		getCommand("unclaim").setExecutor(new UnclaimCommand());
		getCommand("access").setExecutor(new AccessCommand());
		getCommand("showchunk").setExecutor(new ShowChunkCommand());
	}
	
	private void scheduleDataSaver() {
		// From minutes, calculate after how long in ticks to save data.
		int saveTimeTicks = Config.getInt("data", "saveDataInterval") * 60 * 20;
		// Async because possible lag when saving and loading.
		getServer().getScheduler().runTaskTimerAsynchronously(this, this::reloadData, saveTimeTicks, saveTimeTicks);
	}

	private void reloadData() {
		try {
			chunkHandler.writeToDisk();
			playerHandler.writeToDisk();
			
			chunkHandler.readFromDisk();
			playerHandler.readFromDisk();
		} catch (Exception e) {
			e.printStackTrace();
			Utils.log("Couldn't reload data: \"" + e.getMessage() + "\"");
		}
	}

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }
	
	public PlayerHandler getPlayerHandler() {
		return playerHandler;
	}
	
	public ChunkHandler getChunkHandler() {
		return chunkHandler;
	}
	
	public static ClaimChunk getInstance() {
		return instance;
	}

	public void showTitle(Player player, Chunk newChunk) {
		if(ClaimChunk.getInstance().getChunkHandler().isClaimed(newChunk)){
			UUID newOwner = ClaimChunk.getInstance().getChunkHandler().getOwner(newChunk.getWorld(), newChunk.getX(), newChunk.getZ());
			PlayerHandler nh = ClaimChunk.getInstance().getPlayerHandler();
			String newName = nh.getUsername(newOwner);
			BarColor color;
			if(ChunkHelper.canEdit(newChunk.getWorld(), newChunk.getX(), newChunk.getZ(), player.getUniqueId())){
				color = BarColor.GREEN;
			}else{
				color = BarColor.RED;
			}
			BossBarAPI.setBossBar(player, ChatColor.GRAY + "[" + ChatColor.GOLD + ChatColor.BOLD.toString() + newName + ChatColor.GRAY + "]", 1, color, BarStyle.SOLID, null);
		}else{
			BossBarAPI.removeBossBar(player);
		}
	}
	
}