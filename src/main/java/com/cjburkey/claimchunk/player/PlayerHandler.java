package com.cjburkey.claimchunk.player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.bukkit.entity.Player;
import com.cjburkey.claimchunk.data.IDataStorage;
import com.cjburkey.claimchunk.data.JsonDataStorage;
import com.cjburkey.claimchunk.data.SqlDataStorage;

public class PlayerHandler {
	
	private final Queue<DataPlayer> playerData = new ConcurrentLinkedQueue<>();
	private final IDataStorage<DataPlayer> data;
	
	public PlayerHandler(boolean sql, File file) {
		data = (sql) ? new SqlDataStorage<>() : new JsonDataStorage<>(DataPlayer[].class, file);
	}
	
	/**
	 * Toggles the supplied players access to the owner's chunks.
	 * @param owner The chunk owner.
	 * @param player The player to toggle access.
	 * @return Whether or not the player NOW has access.
	 * @throws IOException 
	 */
	public boolean toggleAccess(UUID owner, UUID player) {
		if (hasAccess(owner, player)) {
			takeAccess(owner, player);
			return false;
		}
		giveAccess(owner, player);
		return true;
	}
	
	public void giveAccess(UUID owner, UUID player) {
		if (!hasAccess(owner, player)) {
			DataPlayer a = getPlayer(owner);
			if (a != null) {
				a.permitted.add(player);
			}
		}
	}
	
	public void takeAccess(UUID owner, UUID player) {
		if (hasAccess(owner, player)) {
			DataPlayer a = getPlayer(owner);
			if (a != null) {
				a.permitted.remove(player);
			}
		}
	}
	
	public boolean hasAccess(UUID owner, UUID player) {
		DataPlayer a = getPlayer(owner);
		if (a != null) {
			return a.permitted.contains(player);
		}
		return false;
	}

	public String getUsername(UUID player) {
		DataPlayer a = getPlayer(player);
		if (a != null) {
			return a.lastIgn;
		}
		return null;
	}
	
	public UUID getUUID(String username) {
		for (DataPlayer ply : playerData) {
			if (ply.lastIgn != null && ply.lastIgn.equals(username)) {
				return ply.player;
			}
		}
		return null;
	}
	
	public List<String> getJoinedPlayers(String start) {
		List<String> out = new ArrayList<>();
		for (DataPlayer ply : playerData) {
			if (ply.lastIgn != null && ply.lastIgn.toLowerCase().startsWith(start.toLowerCase())) {
				out.add(ply.lastIgn);
			}
		}
		return out;
	}
	
	public void onJoin(Player ply) {
		if (getPlayer(ply.getUniqueId()) == null) {
			playerData.add(new DataPlayer(ply));
		}
	}
	
	public void writeToDisk() throws Exception {
		data.clearData();
		for (DataPlayer a : playerData) {
			data.addData(a.clone());
		}
		data.saveData();
	}
	
	public void readFromDisk() throws Exception {
		data.reloadData();
		playerData.clear();
		for (DataPlayer ply : data.getData()) {
			playerData.add(ply.clone());
		}
	}
	
	private DataPlayer getPlayer(UUID owner) {
		for (DataPlayer a : playerData) {
			if (a.player.equals(owner)) {
				return a;
			}
		}
		return null;
	}
}